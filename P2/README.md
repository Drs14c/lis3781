> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 Advance Database Management

## DeVon Singleton

### Project 2 Requirements:

#### README.md file should include the following items:

1. Use MongoDb to import information
2. Use MongoDb to view information
3. Use MongoDb to update information
4. Use MongoDb to create a define result set
 

* Bullet-list items
* 
* 
* 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - create a new local repository
2. git status - list the files you have changed and those you still need to add or commit
3. git add - add one or more files to staging(index)
4. git commit - commit changes to head (but not yet the remote repoistory) 
5. git push - send changes to the master branch of your remote repository
6. git pull - fetch and merged changes on the remote server to your working directory
7. git diff - view all the merge conflicts 



#### Assignment Screenshots:

Screenshot of Code							|Screenshot of Code				|
:----------------------------------------------------------------------:|:---------------------------------------------:|
 *Screenshot of Code*:![Screenshot](img/1.png)				|*Screenshot of Code2*:![Screenshot](img/2.png)	|

|Screenshot of Code                             |Screenshot of update|
:----------------------------------------------:|:---------------------------------------------
|*Screenshot of Code3*:![Screenshot](img/3.png) |*Screenshot of Table*:![Screenshot](img/5.png)






#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link]
(https://bitbucket.org/Drs14c/bitbucketstationslocations/src/master/     "Bitbucket Station Locations")


*My Bitbucket Link*
[My Link](https://bitbucket.org/Drs14c/lis3781/src/master/)
