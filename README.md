> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 Advanced Database Managements

## DeVon Singleton

### LIS3781:

*Course Work Links:*

1. [A1 README.md](A1/README.md "My A1 README.md file")
    - Create database for human resource
    - Forward engineer database

2. [A2 README.md](A2/README.md "My A2 README.md file")

    - Create database with two tables
    - Populate both tables
    - Git commands with description
    - Use Microsoft Sql 

3. [A3 README.md](A3/README.md "My A3 README.md file")

    - Create three tables
    - Populate both tables
    - Git commands with description
    - Use ORACLE enviroment 

4. [A4 README.md](A4/README.md "My A4 README.md file")

    - Use MS Sql Server to create tables
    - Use T-sql to populate tables
    - Create diagram for table showing relationships


5. [A5 README.md](A5/README.md "My A5 README.md file")

    - Use MS Sql Server to create tables
    - Use T-Sql to populate tables
    - Use regular expression to limit some of the numbers
    - Create diagram for table showing relationship

6. [P1 README.md](P1/README.md "My P1 README.md file")

    - Use MySQL to create tables for judge-client-representor relationship

    - Use salt for hashing SSN   	

7. [P2 README.md](P2/README.md "My P2 README.md file")

    - Configure MongoDb

    - Use MongoDb to import information

    - Use MongoDb to view, update or delete information.