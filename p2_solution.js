// INSERTS
// 17 Insert a record with following data
 street = 7th Avenue
 zip code = 10024
 building = 1000
 coord = -58.9557413, 31.7720266
 borough = Brooklyn
 cuisine = BBQ
 date = 2015-11-05T00:00:00Z
 grade : C
 score = 15
 name = Big Text
 restaurant_id = 61704627

 //Step 1: review data to display format
 db.restaurants.find().limit(1).pretty()
 
 //Step 2: then insert
 db.restaurants.insert(
     {
         // address has multiple parts

         "address":{
             "street": "7th Avenue",
             "zipcode": "10024",
             "building": "1000",
             "coord" : [-58.9557413, 31.7720266],
            },
            "borough" : "Brooklyn",
            "cuisine" : "BBQ",
         // grades has multiple parts   
            "grades" : [
                {
            "date": ISODate("2015-11-05T00:00:00Z"),
            "grade": "C",
            "score": 15
                }
            ],
            "name": "Big Tex",
            "restaurant_id": "61704627"
     }
 )
