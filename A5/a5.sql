-- MS SQL SERVER

/*
Notes: Tables must include the following constraints and defaults:
* per_ssn must be unique(see indexes/key) and sha2_512 hashed
* per_gender: m or f
* per_type: c or s
example([per_gender]='f'or[per_gender]='m')
*state: default = fl
* zip: require entries in zip column to be 9 digits
example ([per_zip]like '[0-9]'[0-9]'[0-9]'[0-9]'[0-9]'[0-9]'[0-9]'[0-9]'[0-9]')
* phonenumer require entries to be 10 numbers
example([phn_num]like'[0-9]'[0-9]'[0-9]'[0-9]'[0-9]'[0-9]'[0-9]'[0-9]'[0-9]'[0-9]'[0-9])

fk must require on delete cascade on update cascade

use dbo. considered best practice 

http://www.sqlteam.com/article/understanding-the-differences-between-owners-and-schemas-in-sql-server

*/

-- (not the same)but similiar to show warnings

SET ANSI_WARNINGS ON;
GO

-- avoids error that user kept <db connection open>
use master;
go

-- drop existing database if it exists (use *your* username)
if exists(select name from master.dbo.sysdatabases where name = N'drs14c' )
use master;
drop database drs14c;
go


-- create database if not existing
if not exists(select name from master.dbo.sysdatabases where name = N'drs14c')
create database drs14c;
go

use drs14c;
go
-- be sure to create and populate the tables in correct order parents tables then child tables!

-- drop table if exists
-- n = subsequent string may be in unicode(make it portable to use with unicode character)
-- u only look for objects with this name that are tables
-- be sure to use dbo. before all table references



-- --------------------------------------------------------------
-- TABLE Person
-- ------------------------------------------------------------------------------------ 
If object_id (N'dbo.person',N'U') is not null
DROP TABLE dbo.person;
GO

Create TABLE dbo.person
(
per_id SMALLINT NOT NULL identity(1,1),
per_ssn BINARY(64) NULL,
per_salt BINARY(64) NULL,
per_fname varchar(15) not null,
per_lname varchar(30 ) not null,
per_gender char(1) not null check(per_gender in('m','f')),
per_dob date not null,
per_street varchar(30) not null,
per_city varchar(30) not null,
per_state char(2) not null default 'FL',
per_zip int not null check(per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
per_email varchar(100) not null,
per_type char(1) not null check(per_type in('s','c')) ,
per_notes varchar(255) null,
primary key(per_id),

--- make sure ssn and state id are unique
Constraint ux_per_ssn unique nonclustered (per_ssn asc)
);
go
-- ----------------------------------------------------
-- Table phone
-- ------------------------------------------------------------
If object_id(N'dbo.phone', N'u') is not null
DROP TABLE dbo.phone;
go
CREATE TABLE dbo.phone
(
phn_id smallint not null identity(1,1),
per_id smallint not null,
phn_num bigint not null check(phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
phn_type char(1) not null check(phn_type in('h','c','w','f')),
phn_notes VARCHAR(255) NULL DEFAULT NULL,
Primary key (phn_id),


Constraint fk_phone_person
 FOREIGN KEY(per_id)
 references dbo.person(per_id)
 ON DELETE cascade
 ON UPDATE CASCADE
 );
go

-- ----------------------------------------------------
-- Table customer
-- ------------------------------------------------------------
If object_id(N'dbo.customer', N'u') is not null
DROP TABLE dbo.customer;
go
CREATE TABLE dbo.customer
(
per_id smallint not null,
cus_balance decimal(7,2) not null check(cus_balance >=0),
cus_total_sales decimal(7,2)not null check(cus_total_sales >=0),
cus_notes VARCHAR(255) NULL,
Primary key (per_id),


Constraint fk_customer_person
 FOREIGN KEY(per_id)
 references dbo.person(per_id)
 ON DELETE cascade
 ON UPDATE CASCADE
 );
go
-- ----------------------------------------------------
-- Table slsrep
-- ------------------------------------------------------------
If object_id(N'dbo.slsrep', N'u') is not null
DROP TABLE dbo.slsrep;
go
CREATE TABLE dbo.slsrep
(
per_id smallint not null,
srp_yr_sales_goal decimal(8,2) not null check(srp_yr_sales_goal >=0),
srp_ytd_sales decimal(8,2)not null check(srp_ytd_sales >=0),
srp_ytd_comm decimal(7,2)not null check(srp_ytd_comm >=0),
srp_notes VARCHAR(45) NULL,
Primary key (per_id),


Constraint fk_slsrep_person
 FOREIGN KEY(per_id)
 references dbo.person(per_id)
 ON DELETE cascade
 ON UPDATE CASCADE
 );
go
-- ----------------------------------------------------
-- Table srp_hist
-- ------------------------------------------------------------
If object_id(N'dbo.srp_hist', N'u') is not null
DROP TABLE dbo.srp_hist;
go
CREATE TABLE dbo.srp_hist
(
sht_id smallint not null identity(1,1),
per_id smallint not null,
sht_type char(1) not null check(sht_type in('i','u','d')),
sht_modified datetime not null,
sht_modifier varchar(45) not null default system_user,
sht_date date not null default getDate(),
sht_yr_sales_goal decimal(8,2) not null check(sht_yr_sales_goal >=0),
sht_yr_total_sales decimal(8,2)not null check(sht_yr_total_sales >=0),
sht_yr_total_comm decimal(7,2)not null check(sht_yr_total_comm >=0),
sht_notes VARCHAR(255) NULL,
Primary key (sht_id),


Constraint fk_srp_history_slsrep
 FOREIGN KEY(per_id)
 references dbo.slsrep(per_id)
 ON DELETE cascade
 ON UPDATE CASCADE
 );
go



-- ----------------------------------------------------
-- Table contact
-- ------------------------------------------------------------
If object_id(N'dbo.contact', N'u') is not null
DROP TABLE dbo.contact;
go
CREATE TABLE dbo.contact
(
cnt_id int not null ,
per_cid smallint not null,
per_sid smallint not null,
cnt_date datetime not null,
cnt_notes VARCHAR(255) NULL,
Primary key (cnt_id),


Constraint fk_contact_customer
 FOREIGN KEY(per_cid)
 references dbo.customer(per_id)
 ON DELETE cascade
 ON UPDATE CASCADE,

Constraint fk_contact_slsrep
  Foreign key (per_sid)
  References dbo.slsrep(per_id)
  On delete no action
  On update  no action

);
go
/*

Cannot have cascade paths for both fks in contact because record in contact table are linked with customer record and records in contact are link with slsrep table because there are multiple cascading paths from person to contact table never belong to  both customer and slsrep it would be impossible to make contact tables records cascade delete for both customer and slsrep tables because there are multiple cascading paths from person to contact table (one via customer and one via slsrep) cant do both at same time hence ms sql will generate an erro when attempting to create fks 
*/


-- ----------------------------------------------------
-- Table [order] (must use delimiter [] for reserve words)
-- ------------------------------------------------------------
If object_id(N'dbo.[order]', N'u') is not null

DROP TABLE dbo.[order];
go 

CREATE TABLE dbo.[order]
(
ord_id int not null identity(1,1),
cnt_id int not null,

ord_placed_date datetime not null,

ord_filled_date datetime null,
ord_notes VARCHAR(255) NULL,
Primary key (ord_id),


Constraint fk_order_contact
 FOREIGN KEY(cnt_id)
 references dbo.contact(cnt_id)
 ON DELETE cascade
 ON UPDATE CASCADE
 );
go
-- ----------------------------------------------------
-- Table region
-- ------------------------------------------------------------
If object_id(N'dbo.region', N'u') is not null
DROP TABLE dbo.region;
go
CREATE TABLE dbo.region
(
reg_id tinyint not null identity(1,1),
reg_name char(1) not null, -- n,w,s,e

reg_notes VARCHAR(255) NULL,
Primary key (reg_id)
);
go

-- ----------------------------------------------------
-- Table state
-- ------------------------------------------------------------
If object_id(N'dbo.state', N'u') is not null
DROP TABLE dbo.state;
go
CREATE TABLE dbo.state
(
ste_id tinyint not null identity(1,1),
reg_id tinyint not null, 
ste_name CHAR(2) NOT NULL DEFAULT 'FL',
ste_notes VARCHAR(255) NULL,
Primary key (ste_id),

constraint fk_state_region
foreign key (reg_id)

references dbo.region(reg_id)
on delete cascade
on update cascade

);
go


-- ----------------------------------------------------
-- Table city
-- ------------------------------------------------------------
If object_id(N'dbo.city', N'u') is not null
DROP TABLE dbo.city;
go
CREATE TABLE dbo.city
(
cty_id smallint not null identity(1,1),
ste_id TINYINT not null, 
cty_name varchar(30) not null,
cty_notes VARCHAR(255) NULL,
Primary key (cty_id),

CONSTRAINT fk_city_state
foreign key (ste_id)
references dbo.state(ste_id)
on delete cascade
on update cascade
);
go



-- ----------------------------------------------------
-- Table store
-- ------------------------------------------------------------
If object_id(N'dbo.store', N'u') is not null
DROP TABLE dbo.store;
go
CREATE TABLE dbo.store
(
str_id smallint not null identity(1,1),
cty_id SMALLINT not null,
str_name varchar(45) not null,
str_street varchar(30) not null,

str_zip int not null check(str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
str_phone bigint not null check(str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),

str_email varchar(100) not null,
str_url varchar(100) not null,
str_notes VARCHAR(255) NULL,
Primary key (str_id),

CONSTRAINT fk_store_city
 FOREIGN key (cty_id)
 REFERENCES dbo.city(cty_id)
 on delete CASCADE
 on update CASCADE

);
go
-- ----------------------------------------------------
-- Table invoice
-- ------------------------------------------------------------
If object_id(N'dbo.invoice', N'u') is not null
DROP TABLE dbo.invoice;
go
CREATE TABLE dbo.invoice
(
inv_id int not null identity(1,1),
ord_id int not null ,
str_id smallint not null,
inv_date datetime not null,
inv_total decimal(8,2) not null check(inv_total >=0),
inv_paid bit not null,

inv_notes VARCHAR(255) NULL,
Primary key (inv_id),

-- create inner 1:1 relationship with order by making ord_id unique
CONSTRAINT ux_ord_id unique nonclustered (ord_id asc),


Constraint fk_invoice_order
 FOREIGN KEY(ord_id)
 references dbo.[order](ord_id)
 ON DELETE cascade
 ON UPDATE CASCADE,

Constraint fk_invoice_store
  Foreign key (str_id)
  References dbo.store(str_id)
  On delete cascade
  On update cascade
);
go

-- ----------------------------------------------------
-- Table payment
-- ------------------------------------------------------------
If object_id(N'dbo.payment', N'u') is not null
DROP TABLE dbo.payment;
go
CREATE TABLE dbo.payment
(
pay_id int not null identity(1,1),
inv_id int not null,
pay_date datetime not null,
pay_amt decimal(7,2) not null check(pay_amt >=0),
pay_notes VARCHAR(255) NULL,
Primary key (pay_id),


Constraint fk_payment_invoice
 FOREIGN KEY(inv_id)
 references dbo.invoice(inv_id)
 ON DELETE cascade
 ON UPDATE CASCAde
);
go
-- ----------------------------------------------------
-- Table vendor
-- ------------------------------------------------------------
If object_id(N'dbo.vendor', N'u') is not null
DROP TABLE dbo.vendor;
go
CREATE TABLE dbo.vendor
(
ven_id smallint not null identity(1,1),
ven_name varchar(45)not null,
ven_street varchar(30) not null,
ven_city varchar(30) not null,
ven_state char(2) not null default 'Fl',
ven_zip int not null check(ven_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
ven_phone bigint not null check(ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
ven_email varchar(100) not null,
ven_url varchar(100) not null,
ven_notes VARCHAR(255) NULL,
Primary key (ven_id)
);
go



-- ----------------------------------------------------
-- Table product
-- ------------------------------------------------------------
If object_id(N'dbo.product', N'u') is not null
DROP TABLE dbo.product;
go
CREATE TABLE dbo.product
(
pro_id smallint not null identity(1,1),
ven_id smallint not null,
pro_name varchar(30)not null,
pro_descript varchar(45) null,
pro_weight float not null check(pro_weight >= 0),
pro_qoh smallint not null check(pro_qoh >=0),
pro_cost decimal(7,2) not null check (pro_cost >= 0),
pro_price decimal(7,2) not null check (pro_price >= 0),
pro_discount decimal(3,0) null,
pro_notes VARCHAR(255) NULL,
Primary key (pro_id),


Constraint fk_product_vendor
 FOREIGN KEY(ven_id)
 references dbo.vendor(ven_id)
 ON DELETE cascade
 ON UPDATE CASCADE
);
go
-- ----------------------------------------------------
-- Table product_hist
-- ------------------------------------------------------------
If object_id(N'dbo.product_hist', N'u') is not null
DROP TABLE dbo.product_hist;
go
CREATE TABLE dbo.product_hist
(
pht_id int not null identity(1,1),
pro_id smallint not null,
pht_date datetime not null,
pht_cost decimal(7,2) not null check(pht_cost >= 0),
pht_price decimal(7,2) not null check(pht_price >= 0),
pht_discount decimal(3,0) null,
pht_notes VARCHAR(255) NULL,
Primary key (pht_id),


Constraint fk_product_hist_product
 FOREIGN KEY(pro_id)
 references dbo.product(pro_id)
 ON DELETE cascade
 ON UPDATE CASCADE
);
go
-- ----------------------------------------------------
-- Table order_line
-- ------------------------------------------------------------
If object_id(N'dbo.order_line', N'u') is not null
DROP TABLE dbo.order_line;
go
CREATE TABLE dbo.order_line
(
oln_id int not null identity(1,1),
ord_id int not null,
pro_id smallint not null,
oln_qty smallint not null check(oln_qty >= 0),
oln_price decimal(7,2) not null check(oln_price >=0),
oln_notes VARCHAR(45) NULL,
Primary key (oln_id),


Constraint fk_order_line_order
 FOREIGN KEY(ord_id)
 references dbo.[order](ord_id)
 ON DELETE cascade
 ON UPDATE CASCADE,

Constraint fk_order_line_product
 FOREIGN KEY(pro_id)
 references dbo.product(pro_id)
 ON DELETE cascade
 ON UPDATE CASCADE


);
GO
-- ----------------------------------------------------
-- Table time
-- ------------------------------------------------------------
If object_id(N'dbo.time', N'u') is not null
DROP TABLE dbo.time;
go
CREATE TABLE dbo.time
(
tim_id int not null identity(1,1),
tim_yr smallint not null, -- 2 bytes integer (no year data type in ms sql server)
tim_qtr TINYINT not null, -- 1-4
tim_month tinyint not null, -- 1-12
tim_week tinyint not null, -- 1-52
tim_day tinyint not null, -- 1-7
tim_time time not null, -- based on 24 hours clock
tim_notes VARCHAR(255) NULL,
Primary key (tim_id)
);
go


-- ----------------------------------------------------
-- Table sale
-- ------------------------------------------------------------
If object_id(N'dbo.sale', N'u') is not null
DROP TABLE dbo.sale;
go
CREATE TABLE dbo.sale
(
pro_id smallint not null,
str_id smallint not null,
cnt_id int not null,
tim_id int not null,
sal_qty smallint not null,
sal_price decimal(8,2) not null,
sal_total decimal(8,2) not null,
sal_notes VARCHAR(255) NULL,
Primary key (pro_id,str_id,cnt_id,tim_id),

-- make sure combination of time contact store and product are unique

constraint ux_pro_id_str_id_cnt_id_tim_id
unique nonclustered(pro_id asc,str_id asc, cnt_id asc, tim_id asc),

constraint fk_sale_product
foreign key (pro_id)
references dbo.product(pro_id)
on delete CASCADE
on update cascade,

constraint fk_sale_time
foreign key (tim_id)
references dbo.time(tim_id)
on delete CASCADE
on update cascade,

constraint fk_sale_contact
foreign key (cnt_id)
references dbo.contact(cnt_id)
on delete CASCADE
on update cascade,

constraint fk_sale_store
foreign key (str_id)
references dbo.store(str_id)
on delete CASCADE
on update cascade





);
go



Select * from information_schema.tables;


/*
-- converts to binary
Select hashbytes('SHA2_512','test'); 
-- lengths 64 bytes
Select len(hashbytes('SHA2_512','test')); 
*/


Insert into dbo.person
(per_ssn,per_salt,per_fname,per_lname,per_gender,per_dob,per_street,per_city,per_state,per_zip,per_email,per_type,per_notes)
Values

( 1, NULL, 'Steve', 'Rogers','m','1923-10-03','437 Southern drive', 'Rochester','NY','123456789','sroger@comcast.net','s',null),
( 2, NULL, 'Bruce', 'Wayne','m','1968-03-20','1007 Mountain drive', 'Gotham','NY','123456789','bwayne@knology.net','s',null),
( 3, NULL, 'Peter', 'Parker','m','1988-09-12','510 Ingram Street', 'New York','NY','123456789','pparker@msn.com','s',null),
( 4, NULL, 'Jane', 'Thompson','f','1978-05-08','13563 Ocean View Drive', 'Seattle','WA','123456789','jthompson@gmail.com','s',null),
( 5, NULL, 'Debra', 'Steele','f','1994-07-19','543 Oak Ln', 'Milwaukee','WI','123456789','dsteele@verizon.net','s',null),
( 6, NULL, 'Tony', 'Stark','m','1972-05-04','332 Palm Avenue', 'Malibu','CA','123456789','tstark@yahoo.com','c',null),
( 7, NULL, 'Hank', 'Pymi','m','1980-08-28','2335 Brown Street', 'Cleveland','OH','123456789','hpym@aol.com','c',null),
( 8, NULL, 'Bob', 'Best','m','1992-02-10','4902 Avendale Avenue', 'Scottsdale','AZ','123456789','bbest@yahoo.com','c',null),
( 9, NULL, 'Sandra', 'Dole','f','1990-01-26','87912 Lawrence Ave', 'Atlanta','GA','123456789','sdole@gmail.com','c',null),
( 10, NULL, 'Ben', 'Avery','m','1983-12-24','6432 Thuderbird Ln', 'Sioux Falls','SD','123456789','bavery@hotmail.com','c',null),
( 11, NULL, 'Arthur', 'Curry','m','1923-10-03','3304 Euclid Avenue', 'Miami','FL','123456789','acurry@gmail.com','c',null),
( 12, NULL, 'Diana', 'Price','f','1980-08-03','944 Green Street', 'Las Vegas','NV','123456789','dprice@symaptico.com','c',null),
( 13, NULL, 'Adam', 'Jurris','m','1995-01-31','98435 Valencia Dr', 'Gulf Shore','AL','123456789','ajurrix@gmx.com','c',null),
( 14, NULL, 'Judy', 'Sleen','f','1970-03-22','56343 Rover Ct.', 'Billings','MT','123456789','jsleen@symaptico.com','c',null),
( 15, NULL, 'Bill', 'Niederheim','m','1982-03-13','433567 Netherland Blvd', 'South Bend','IN','123456789','bheiderheim@comcast.net','c',null);

go
select * from dbo.person;




-- -------------------------------------
-- Data for dbo.slsrep
-- ---------------------------------------
insert into dbo.slsrep
(per_id,srp_yr_sales_goal,srp_ytd_sales,srp_ytd_comm,srp_notes)
Values
(1,1000,60000,1800,null),
(2,800,35000,3500,null),
(3,1500,84000,9650,'Great sales person'),
(4,1250,87000,15300,null),
(5,900,43000,8750,null);
go
select * from dbo.slsrep;


-- -------------------------------------
-- Data for dbo.customer
-- ---------------------------------------
insert into dbo.customer
(per_id,cus_balance,cus_total_sales,cus_notes)
Values
(6, 120, 14789,null),
(7, 98.46, 234.92,null),
(8, 10, 4578,'Customer always pays on time.'),
(9, 981.73, 1672.38,'High balance'),
(10, 541.23, 782.57,null),
(11, 251.02, 13782.96,'Good customer'),
(12, 582.67, 963.12,'Previously paid in full'),
(13, 121.67, 1057.45,'Recent Customer .'),
(14, 765.43, 6789.42,'Buys bulk quantities'),
(15, 304.39, 456.81,'Has not purchased recently');
go
select * from dbo.customer;

-- -------------------------------------
-- Data for dbo.contact
-- ---------------------------------------
insert into dbo.contact
(cnt_id,per_sid,per_cid,cnt_date,cnt_notes)
Values
(1,1,6,'1999-01-01',null),
(2,2,6,'2001-09-29',null),
(3,3,7,'2002-08-15',null),
(4,2,7,'2002-09-01',null),
(5,4,7,'2004-01-05',null),
(6,5,8,'2004-02-28',null),
(7,4,8,'2004-03-03',null),
(8,1,9,'2004-04-07',null),
(9,5,9,'2004-07-29',null),
(10,3,11,'2005-05-02',null),
(11,4,13,'2005-06-14',null),
(12,3,11,'2005-07-02',null);

GO
select * from dbo.contact;

-- -------------------------------------
-- Data for dbo.[order]
-- ---------------------------------------
insert into dbo.[order]
(cnt_id,ord_placed_date,ord_filled_date,ord_notes)
Values
(1,'2010-11-23','2010-12-24',null),
(2,'2005-03-19','2005-07-28',null),
(3,'2011-07-01','2011-07-06',null),
(4,'2009-12-24','2010-01-05',null),
(5,'2008-09-21','2008-11-26',null),
(1,'2009-04-17','2009-04-30',null),
(2,'2010-05-31','2010-06-07',null),
(3,'2007-09-02','2007-09-16',null),
(4,'2011-12-08','2011-12-23',null),
(5,'2012-02-29','2012-05-02',null);

go

select * from dbo.[order];

-- ------------------------------------
-- data for dbo.region
-- ------------------------------------
insert into region
(reg_name, reg_notes)
VALUES
('c',null),
('n', null),
('e',null),
('s', null),
('w',null);
GO
select * from dbo.region;

-- --------------------------------------
-- data for table state
-- --------------------------------------
insert into state
(reg_id,ste_name, ste_notes)
VALUES
(1,'MI',null),
(2,'IL', null),
(3,'WA',null),
(4,'FL', null),
(5,'TX',null);
GO
select * from dbo.state;


-- --------------------------------------
-- data for table city
-- --------------------------------------
insert into city
(ste_id,cty_name, cty_notes)
VALUES
(1,'Detroit',null),
(2,'Aspen', null),
(2,'Chicago',null),
(3,'Clover', null),
(4,'St. Louis',null);
GO
select * from dbo.city;




-- -------------------------------------
-- Data for dbo.store
-- ---------------------------------------
insert into dbo.store
(cty_id,str_name,str_street,str_zip,str_phone,str_email,str_url,str_notes)
Values
(2,'Walgreens','148567 Walnut Ln','475315690','3127658127','info@walgreens.com','http://www.walgreens.Customer',null),
(3,'CVS','572 Casper RD','505231519','3128926534','help@cvs.com','http://www.CVS.com','Rumor of merger'),
(4,'Lowes','81309 Castapault Ave','802345671','9010765342','sales@lowes.com','http://www.lowes.com',null),
(5,'Walmart','148567 Walnut Ln','387563628','8722718923','info@walmart.com','http://www.walmart.com',null),
(1,'Dollar General','47583 Davison Rd','482983456','3137583492','ask@dollargeneral.com','http://www.dollargeneral.com',null);

go
select * from dbo.store;


-- -------------------------------------
-- Data for dbo.invoice
-- ---------------------------------------
insert into dbo.invoice
(ord_id,str_id,inv_date,inv_total,inv_paid,inv_notes)
Values
(5,1,'2001-05-03',58.32,0,null),
(4,1,'2006-11-11',100.59,0,null),
(1,1,'2010-09-16',57.34,0,null),
(3,2,'2011-01-10',99.32,1,null),
(2,3,'2008-06-24',1109.67,1,null),
(6,4,'2009-04-20',239.83,0,null),
(7,5,'2010-06-05',537.29,0,null),
(8,2,'2007-09-09',644.21,1,null),
(9,3,'2011-12-17',934.12,1,null),
(10,4,'2012-03-18',274.45,0,null);
go 

select * from dbo.invoice;

-- -------------------------------------
-- Data for dbo.vendor
-- ---------------------------------------
insert into dbo.vendor
(ven_name,ven_street,ven_city,ven_state,ven_zip,ven_phone,ven_email,ven_url,ven_notes)
Values
('Sysco','531 Dolphin Run', 'Oriana','FL','344761234','7641238543','sales@sysco.com','http://www.sysco.com',null),
('General Eletric','100 Happy Trails Dr', 'Boston','MA','123458743','2134569641','support@ge.com','http://www.ge.com','Very good turnaround'),
('Cisco','300 Cisco Dr', 'Standford','OR','872315492','7823456723','cisco@cisco.com','http://www.cisco.com',null),
('GoodYear','100 GoodYear Dr', 'Gary','IN','485321956','5784218427','sales@goodyear.com','http://www.goodyear.com','Competing well with FireStone.'),
('Snap-On','42185 Magneta Ave', 'Lake Falls','ND','387513649','9197345632','support@snapon.com','http://www.snap-on.com','Good qualities of tool');
GO
select * from dbo.vendor;

-- -------------------------------------
-- Data for dbo.product
-- ---------------------------------------
insert into dbo.product
(ven_id,pro_name,pro_descript,pro_weight,pro_qoh,pro_cost,pro_price,pro_discount,pro_notes)
Values
(1,'hammer','claw type',2.5,45,4.99,7.99,30,'Discounted only when purchased with screwdriver set'),
(2,'screwdriver','philip',1.8,120,1.99,3.49,null,null),
(3,'pail','red pail',2.8,48,3.89,7.99,40,null),
(4,'cooking oil','whale',15,19,19.99,28.99,null,'gallons'),
(5,'frying pan','silver pan',3.5,178,8.45,13.99,50,'Currently 1/2 off price sale.');
GO
select * from dbo.product;

-- -------------------------------------
-- Data for dbo.order_line
-- ---------------------------------------
insert into dbo.order_line
(ord_id,pro_id,oln_qty,oln_price,oln_notes)
Values
(1,2,10,8.0,null),
(2,3,10,9.88,null),
(3,4,3,6.99,null),
(5,1,2,12.76,null),
(4,5,13,58.99,null);
GO
select * from dbo.order_line;

-- -------------------------------------
-- Data for dbo.payment
-- ---------------------------------------
insert into dbo.payment
(inv_id,pay_date,pay_amt,pay_notes)
Values
(5,'2008-07-01',5.99,null),
(4,'2010-09-28',4.99,null),
(1,'2008-07-23',8.75,null),
(3,'2010-10-31',19.55,null),
(2,'2011-03-29',32.5,null),
(6,'2010-10-03',20.00,null),
(8,'2008-08-09',1000.00,null),
(9,'2009-01-10',103.68,null),
(7,'2007-03-15',25.00,null),
(10,'2007-05-12',40.00,null),
(4,'2007-05-22',9.33,null);
go
select * from dbo.payment



-- -------------------------------------
-- Data for dbo.product_hist
-- ---------------------------------------
insert into dbo.product_hist
(pro_id,pht_date,pht_cost,pht_price,pht_discount,pht_notes)
Values
(1,'2005-01-02 11:53:34',4.99,7.99,30,'Discounted only when purchased with screwdriver set'),
(2,'2005-02-03 09:13:56',1.99,3.49,null,null),
(3,'2005-03-04 23:21:49',3.89,7.99,null,null),
(4,'2006-05-06 18:09:04',19.99,28.99,32,'gallons'),
(5,'2006-05-07 15:07:29',8.45,13.99,50,'Currently 1/2 off price sale.');
GO
select * from dbo.product_hist;

-- -------------------------------------
-- Data for dbo.time
-- ---------------------------------------
insert into dbo.time
(tim_yr,tim_qtr,tim_month,tim_week,tim_day,tim_time,tim_notes)
Values
(2008,2,5,19,7,'11:59:59',null),
(2010,4,12,49,4,'08:34:21',null),
(1999,4,12,52,5,'05:21:34',null),
(2011,3,8,36,1,'09:32:18',null),
(2001,3,7,27,2,'23:56:32',null),
(2010,2,4,14,5,'02:49:11',null),
(2014,1,2,8,2,'12:27:14',null),
(2013,3,9,38,4,'10:12:28',null),
(2012,4,11,47,3,'22:36:22',null),
(2014,2,6,23,3,'19:07:10',null);
GO
select * from dbo.time;
/*
-- parsing date into integer for qtr, month, day, week
select datepart(q,'2008-05-11');
select datepart(m,'2008-05-11');
select datepart(ww,'2008-05-11');
select datepart(dw,'2008-05-11');

-- Research the following ms sql server date features:
datepart(), datetime(), and set datefirst 7;


*/

-- -------------------------------------
-- Data for dbo.sale
-- ---------------------------------------
insert into dbo.sale
(pro_id, str_id,cnt_id,tim_id,sal_qty,sal_price,sal_total,sal_notes )
Values
(1,5,5,3,20,9.99,199.8,null),
(2,4,6,2,5,5.99,29.95,null),
(3,3,4,1,30,3.99,119.7,null),
(4,2,1,5,15,18.99,284.85,null),
(5,1,2,4,6,11.99,71.94,null),
(5,2,5,6,10,9.99,199.8,null),
(4,3,6,7,5,5.99,29.95,null),
(3,1,4,8,30,3.99,119.7,null),
(2,3,1,9,15,18.99,284.85,null),
(1,4,2,10,6,11.99,71.94,null),
(1,2,3,9,10,11.99,119.8,null),
(5,1,1,8,12,10.99,131.88,null),
(2,3,12,4,1,9.99,9.99,null),
(3,2,12,5,30,1.00,30.0,null),
(2,1,6,6,30,3.00,90.0,null),
(1,1,1,3,20,9.99,199.8,null),
(5,1,12,10,10,10.0,100.0,null),
(3,2,1,4,20,10.0,200.0,null),
(3,1,4,2,15,5.0,75.0,null),
(1,2,3,1,20,9.0,180.0,null),
(2,2,4,7,6,5.00,30.00,null),
(1,2,3,4,6,4.99,29.94,null),
(3,2,1,7,10,10.00,100.0,null),
(2,2,1,7,5,9.99,49.95,null),
(3,1,5,2,10,10.0,100.0,null),
(4,5,2,7,10,5.0,50.0,null);
GO

select * from dbo.sale;


-- -------------------------------------
-- Data for dbo.srp_hist
-- ---------------------------------------
insert into dbo.srp_hist
(per_id,sht_type,sht_modified,sht_modifier,sht_date,sht_yr_sales_goal,sht_yr_total_sales,sht_yr_total_comm,sht_notes)
Values
(1,'i',getDate(),system_user,getDate(),100000,110000,11000,null),
(2,'i',getDate(),system_user,getDate(),150000,175000,17500,null),
(3,'u',getDate(),system_user,getDate(),100000,110000,18500,null),
(4,'u',getDate(),original_login(),getDate(),210000,220000,22000,null),
(5,'i',getDate(),original_login(),getDate(),225000,230000,2300,null);
GO

select * from dbo.srp_hist;

-- -------------------------
-- Table phone
-- -------------------------
insert into dbo.phone
(per_id,phn_num,phn_type,phn_notes)
Values
(1,8507654356,'w',null),
(1,8505554356,'h','Can reach at this number after 6pm'),
(2,1117654356,'f','Send with sender name'),
(4,8991254356,'c','can text or car'),
(3,2547654356,'w',null),
(5,2147654356,'w','reachable at 9-5'),
(6,9187654356,'w',null),
(7,2197654356,'h',null),
(8,9127654356,'c',null),
(9,8507654356,'f','no fax after 8pm'),
(10,8507654356,'h',null),
(11,8507654356,'w',null);
go
select * from dbo.phone;

go
CREATE PROCEDURE dbo.CreatePersonSSN
as

Begin

  declare @salt binary(64);
  declare @ran_num int;
  declare @ssn binary(64);
  declare @x int, @y int;
  set @x =1 ;

  -- dynamically set loop ending value (total number of persons)

  set @y=(select count(*) from dbo.person);

  -- select @y; display number of persons for testing

  while(@x <= @y)
  Begin
  
  -- give each person a unique randonmized salt and hasshed and salted randomnized ssn
  -- note this demo is only for showing how to include salted and hashed randomnized values for testing purposes
  -- function returns a crytographic randomnly-generated hexadecimal number with lengths of specified number of bytes
  -- https://docs.microsoft.com/en-us/sql/t-sql/functions/crypt-gen-random-transact-sql?view=sql-server-2017

  set @salt=crypt_gen_random(64);-- salt includes unique random bytes for each user when looping
  set @ran_num=floor(rand()*(999999999-111111111+1))+111111111;-- random 9-digits ssn from 111111111-99999999
  set @ssn=hashbytes('SHA2_512', concat(@salt, @ran_num));

  -- select @salt, len(@salt),@ran_num,len(@ran_num),@ssn,len(@ssn); -- only for testing values
  -- rand([n]): returns random floating point value v in the range 0 < = v < 1.0
  -- Documentation :https://www.technothenet.com/sql_server/functions/rand.pht_price
  -- randomnized ssn between 11111111-999999999 notes using values 000000000 for ex 4 and below


update dbo.person
set per_ssn=@ssn,per_salt=@salt
where per_id=@x;

set @x = @x + 1;

end;
end;
go

exec dbo.CreatePersonSSN

-- get year only from srp_hist table

select year(sht_date) from dbo.srp_hist;

/*
system_user presents you with the credential used to run the query.
This is important to establish which permission were active

original_login is giving you the user with which the connection was established.
This is also important information.

system_user 
Pro: you can see with which permissions a query was executed
Con: you dont know with which permissions the query was executed 

*/

-- http://www.differencebetween.net/business/difference-between-purchase-order-and-invoice/


-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Begin Report %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- MS SQLServer
-- list tables
-- select * from [table name].information_schema.tables;

select * from [drs14c].information_schema.tables;


-- metadata of database tables
select * from [drs14c].information_schema.columns;


-- summary information of objects
-- sp_help 'object_name'

-- summary information of object dbo.srp_hist

--sp_help dbo.srp_hist;
-- go     

select * from dbo.person;


