/* Save the following file as p1
_solution  
-- create and populate database and tables*/

-- ##############################Create database and Tables####################################################33333
-- see data output after insert statements below
select 'drop,create,use database, create tables, display data: 'as '';
do sleep(5); -- pause mysql console output
DROP SCHEMA IF EXISTS drs14c;
Create SCHEMA IF NOT EXISTS drs14c;
SHOW WARNINGS;
use drs14c;
-- --------------------------------------------------------------
-- TABLE Person
-- ----------------------------------------------------------------------------------
-- Noteallow per_ssn to be null,in order to use stor proc createpersonssn
DROP TABLE IF EXISTS person;
Create TABLE IF NOT EXISTS person
(
per_id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL,
per_ssn BINARY(64) NULL,
per_salt BINARY(64) NULL,
per_fname varchar(15) not null,
per_lname varchar(30 ) not null,
per_street varchar(30) not null,
per_city varchar(30) not null,
per_state char(2) not null,
per_zip int(9) unsigned  zerofill not null,
per_email varchar(100) not null,
per_dob date not null,
per_type enum('a','c','j') not null,
per_notes varchar(255) null,
primary key(per_id),
UNIQUE INDEX ux_per_ssn(per_ssn ASC)
)
ENGINE=InnoDB 
COLLATE = utf8_unicode_ci;
SHOW WARNINGS;

-- ----------------------------------------------------
-- Table attorney
-- ------------------------------------------------------------
DROP TABLE If EXISTS attorney;
CREATE TABLE IF NOT EXISTS attorney
(
per_id smallint unsigned not null,
aty_start_date date not null,
aty_end_date date null default null,
aty_hourly_rate decimal(5,2) unsigned not null,
aty_years_in_practice TINYINT NOT NULL,
aty_notes VARCHAR(255) NULL DEFAULT NULL,
Primary key (per_id),

INDEX idx_per_id(per_id asc),

Constraint fk_attorney_person
 FOREIGN KEY(per_id)
 references person(per_id)
 ON DELETE NO ACTION
 ON UPDATE CASCADE
 )
 ENGINE = InnoDB
 DEFAULT CHARACTER SET = utf8
 COLLATE= utf8_unicode_ci;
 
 SHOW WARNINGS;
  -- --------------------------------------------------
 -- TABLE CLIENT
 -- ---------------------------------------------------------------------
 Drop table if exists client ;
 create table if not exists `client`
 (
 per_id SMALLINT unsigned not null,
 cli_notes VARCHAR(255) NULL default NULL,
 primary key (per_id),
 
 INDEX idx_per_id(per_id ASC),

 constraint fk_client_person
  foreign key(per_id)
  references person(per_id)
  ON DELETE NO ACTION
  ON UPDATE CASCADE
  )
  
  ENGINE = InnoDB
  DEFAULT character SET =utf8
  collate = utf8_unicode_ci;
  SHOW WARNINGS;
  -- ----------------------------------------------
  -- Table court
  -- -----------------------------------------------
 drop table if exists court ;
 create table if not exists court
 (
 crt_id tinyint unsigned auto_increment,
 crt_name varchar(45) not null,
 crt_street varchar(30) not null,
 crt_city varchar(30) not null,
 crt_state char(2) not null,
 crt_zip int(9) unsigned zerofill not null,
 crt_phone bigint not null,
 crt_email varchar(100) not null,
 crt_url varchar(100) not null,
 crt_notes varchar(255) null,
  primary key (crt_id)
 )
 Engine = InnoDB
 default character set=utf8
 Collate = utf8_unicode_ci;
 SHOW warnings;
 
 -- ----------------------------------------------------------------
 -- Table judge
 -- -----------------------------------------------------------------
 drop table if exists judge;
 create table if not exists judge
 (
 per_id smallint unsigned Not null,
 crt_id tinyint UNSIGNED Null default null,
 jud_salary decimal(8,2)not null,
 jud_years_in_practice tinyint unsigned not null,
 jud_notes varchar(255) null default null,
 primary key (per_id),
 
 index idx_per_i(per_id asc),
 index idx_crt_id (crt_id asc),
 
 constraint fk_judge_person
  foreign key (per_id)
  references person(per_id)
  On delete no action
  On update cascade,
 
 constraint fk_judge_court
  foreign key (crt_id)
  references court(crt_id)
  On delete no action
  On update cascade
 )

Engine= InnoDb
Default Character Set = utf8
COllate = utf8_unicode_ci;

SHOW WARNINGS;

-- --------------------------------------------------
-- TABLE judge_hist
-- -----------------------------------------------------
drop table if exists judge_hist;
create table if not exists judge_hist
(
jhs_id smallint unsigned not null auto_increment,
per_id smallint unsigned not null,
jhs_crt_id tinyint null,
jhs_date timestamp not null default current_timestamp(),
jhs_type enum('i','u','d') default 'i',
jhs_salary decimal(8,2) not null,
jhs_notes varchar(255) null,
primary key (jhs_id),

index idx_per_id (per_id asc),

constraint fk_judge_hist_judge
 foreign key (per_id)
 references judge(per_id)
 on delete no action
 on update cascade
 
)
engine =InnoDB
default character set = utf8
collate= utf8_unicode_ci;

Show warnings;

-- --------------------------------------------------
-- TABLE case
-- -----------------------------------------------------
drop table if exists `case`;
create table if not exists `case`
(
cse_id smallint unsigned not null auto_increment,
per_id smallint unsigned not null,
cse_type varchar(45) not null,
cse_description text not null ,
cse_start_date date not null,
cse_end_date date null,
cse_notes varchar(255) null,
primary key (cse_id),

index idx_per_id (per_id asc),

constraint fk_court_case_judge
 foreign key (per_id)
 references person(per_id)
 on delete no action
 on update cascade
 
)
engine =InnoDB
default character set = utf8
collate= utf8_unicode_ci;

Show warnings;

-- --------------------------------------------------
-- TABLE bar
-- -----------------------------------------------------
drop table if exists bar;
create table if not exists bar
(
bar_id tinyint unsigned not null auto_increment,
per_id smallint unsigned not null,
bar_name varchar(45) not null,
bar_notes varchar(255) null,
primary key (bar_id),

index idx_per_id (per_id asc),

constraint fk_bar_attorney
 foreign key (per_id)
 references attorney(per_id)
 on delete no action
 on update cascade
 
)
engine =InnoDB
default character set = utf8
collate= utf8_unicode_ci;

Show warnings;

-- --------------------------------------------------
-- TABLE specialty
-- -----------------------------------------------------
drop table if exists specialty;
create table if not exists specialty
(
spc_id tinyint unsigned not null auto_increment,
per_id smallint unsigned not null,
spc_type varchar(45) not null,
spc_notes varchar(255) null,
primary key (spc_id),

index idx_per_id (per_id asc),

constraint fk_specialty_attorney
 foreign key (per_id)
 references attorney(per_id)
 on delete no action
 on update cascade
 
)
engine =InnoDB
default character set = utf8
collate= utf8_unicode_ci;

Show warnings;

-- --------------------------------------------------
-- TABLE assignment
-- -----------------------------------------------------
drop table if exists assignment;
create table if not exists assignment
(
asn_id smallint unsigned not null auto_increment,
per_cid smallint unsigned not null,
per_aid smallint unsigned not null,
cse_id smallint unsigned not null ,
asn_notes varchar(255) null,
primary key (asn_id),

index idx_per_cid (per_cid asc),
index idx_per_aid (per_aid asc),
index idx_cse_id (cse_id asc),

unique index ux_per_cid_ux_per_aid_ux_cse_id(per_cid asc, per_aid asc, cse_id asc) ,


constraint fk_assign_case
 foreign key (cse_id)
 references `case`(cse_id)
 on delete no action
 on update cascade,
 
constraint fk_assignment_client
 foreign key (per_cid)
 references client (per_id)
 on delete no action
 on update cascade,
 
constraint fk_assignment_attorney
 foreign key (per_aid)
 references attorney(per_id)
 on delete no action
 on update cascade
 )
engine =InnoDB
default character set = utf8
collate= utf8_unicode_ci;

Show warnings;

-- --------------------------------------------------
-- TABLE phone
-- -----------------------------------------------------
drop table if exists phone;
create table if not exists phone
(
phn_id smallint unsigned not null auto_increment,
per_id smallint unsigned not null,
phn_num bigint unsigned not null,
phn_type enum('h','c','w','f') not null comment 'home,cell,work,facts',
phn_notes varchar(255) null,
primary key (phn_id),

index idx_per_id (per_id asc),

constraint fk_phone_person
 foreign key (per_id)
 references person(per_id)
 on delete no action
 on update cascade
 )
engine =InnoDB
default character set = utf8
collate= utf8_unicode_ci;

Show warnings;

-- -------------------------
-- Data for person table
-- -------------------------
start transaction;
insert into person
(per_id,per_ssn,per_salt,per_fname,per_lname,per_street,per_city,per_state,per_zip,per_email,per_dob,per_type,per_notes)
values
(NULL, NULL, NULL, 'Steve', 'Rogers','437 Southern drive', 'Rochester','NY',324402222,'sroger@comcast.net','1923-10-03','c',null),
(NULL, NULL, NULL, 'Bruce', 'Wayne','1007 Mountain drive', 'Gotham','NY',003208440,'bwayne@knology.net','1968-03-20','c',null),
(NULL, NULL, NULL, 'Peter', 'Parker','510 Ingram Street', 'New York','NY',102862341,'pparker@msn.com','1988-09-12','c',null),
(NULL, NULL, NULL, 'Jane', 'Thompson','13563 Ocean View Drive', 'Seattle','WA',03084409,'jthompson@gmail.com','1978-05-08','c',null),
(NULL, NULL, NULL, 'Debra', 'Steele','543 Oak Ln', 'Milwaukee','WI',286234178,'dsteele@verizon.net','1994-07-19','c',null),
(NULL, NULL, NULL, 'Tony', 'Stark','332 Palm Avenue', 'Malibu','CA',902638332,'tstark@yahoo.com','1972-05-04','a',null),
(NULL, NULL, NULL, 'Hank', 'Pymi','2335 Brown Street', 'Cleveland','OH',022348890,'hpym@aol.com','1980-08-28','a',null),
(NULL, NULL, NULL, 'Bob', 'Best','4902 Avendale Avenue', 'Scottsdale','AZ',872638332,'bbest@yahoo.com','1992-02-10','a',null),
(NULL, NULL, NULL, 'Sandra', 'Dole','87912 Lawrence Ave', 'Atlanta','GA',002348890,'sdole@gmail.com','1990-01-26','a',null),
(NULL, NULL, NULL, 'Ben', 'Avery','6432 Thuderbird Ln', 'Sioux Falls','SD',562638332,'bavery@hotmail.com','1983-12-24','a',null),
(NULL, NULL, NULL, 'Arthur', 'Curry','3304 Euclid Avenue', 'Miami','FL',000219932,'acurry@gmail.com','1923-10-03','j',null),
(NULL, NULL, NULL, 'Diana', 'Price','944 Green Street', 'Las Vegas','NV',332048823,'dprice@symaptico.com','1980-08-03','j',null),
(NULL, NULL, NULL, 'Adam', 'Jurris','98435 Valencia Dr', 'Gulf Shore','AL',870219932,'ajurrix@gmx.com','1995-01-31','j',null),
(NULL, NULL, NULL, 'Judy', 'Sleen','56343 Rover Ct.', 'Billings','MT',672048823,'jsleen@symaptico.com','1970-03-22','j',null),
(NULL, NULL, NULL, 'Bill', 'Niederheim','433567 Netherland Blvd', 'South Bend','IN',320219932,'bheiderheim@comcast.net','1982-03-13','j',null);

commit;

-- ----------------------------------
-- Data for table phone
-- ---------------------------------------
Start transaction;
insert into phone
(phn_id,per_id,phn_num,phn_type,phn_notes)
values
(NULL,1,8032288827,'c',null),
(NULL,2,2052338293,'h',null),
(NULL,4,8032288827,'w','has two office phone numbers'),
(NULL,5,6402338494,'w',null),
(NULL,6,5508329842,'f','fax number not currently working'),
(NULL,7,8202052203,'c','prefers home calls'),
(NULL,8,4008338294,'h',null),
(NULL,9,7654328912,'w',null),
(NULL,10,5463721984,'f','work fax number'),
(NULL,11,4537821902,'h','prefers cell phone calls'),
(NULL,12,7867821902,'h','prefers cell phone calls'),
(NULL,13,4537821654,'w','call during lunch'),
(NULL,14,3721821902,'c','prefers cell phone calls'),
(NULL,15,9217821945,'f','use for faxing legal docs');

commit;

-- ---------------------------------
-- Data for table client 
-- ---------------------------------
Start transaction ;
Insert into client
(per_id,cli_notes)
values
(1,null),
(2,null),
(3,null),
(4,null),
(5,null);

commit;

-- ---------------------------------
-- Data for table attorney
-- ---------------------------------
Start transaction ;
Insert into attorney
(per_id,aty_start_date,aty_end_date,aty_hourly_rate,aty_years_in_practice,aty_notes)
values
(6,'2006-06-12',null,85,5,null),
(7,'2003-08-20',null,130,28,null),
(8,'2009-12-12',null,70,17,null),
(9,'2008-06-08',null,78,13,null),
(10,'2011-09-12',null,60,24,null);

commit;


-- ---------------------------------
-- Data for table bar
-- ---------------------------------
Start transaction ;
Insert into bar
(bar_id,per_id,bar_name,bar_notes)
values
(null,6,'Florida Bar',null),
(null,7,'Alabama Bar',null),
(null,8,'Georgia Bar',null),
(null,9,'Michigan Bar',null),
(null,10,'South Carolina Bar',null),
(null,6,'Montana Bar',null),
(null,7,'Arizona Bar',null),
(null,8,'Nevada Bar',null),
(null,9,'New York Bar',null),
(null,10,'New York Bar',null),
(null,6,'Mississippi Bar',null),
(null,7,'California Bar',null),
(null,8,'Illinios Bar',null),
(null,9,'Indiana Bar',null),
(null,10,'Illinios Bar',null),
(null,6,'Tallahassee Bar',null),
(null,7,'Ocala Bar',null),
(null,8,'Bay County Bar',null),
(null,9,'Cincinatti Bar',null);
commit;


-- ----------------------------------
-- Data for table specialty
-- ---------------------------------------
Start transaction;
insert into specialty
(spc_id,per_id,spc_type,spc_notes)
values
(NULL,6,'business',null),
(NULL,7,'trafic',null),
(NULL,8,'bankrupcty',null),
(NULL,9,'insurance',null),
(NULL,10,'judicial',null),
(NULL,6,'environmental', null),
(NULL,7,'criminal',null),
(NULL,8,'real estate',null),
(NULL,9,'malpractice',null);

commit;

-- ----------------------------------
-- Data for table court
-- ---------------------------------------
Start transaction;
insert into court
(crt_id,crt_name,crt_street,crt_city,crt_state,crt_zip,crt_phone,crt_email,crt_url,crt_notes)
values
(NULL,'Leon county circuit court','301 south monroe st', 'Tallahassee', 'Fl',323035292, 8506065504,'lccc@us.fl.gov','leoncountycircuitcourt.gov/',null),
(NULL,'Leon county traffic court','1921 Thomasville Rd', 'Tallahassee', 'Fl',323035292, 8505774100,'lctc@us.fl.gov','leoncountytrafficcourt.gov/',null),
(NULL,'florida surpreme court','500 south duval street', 'Tallahassee', 'Fl',323035292, 8504880125,'fsc@us.fl.gov','http://www.floridasupremecourt.org/',null),
(NULL,'orange country courthouse','424 north orange ave', 'orlando', 'Fl',323035292, 4078362000,'nthcircuit@us.fl.gov','http://www.ninthcircuit.org/',null),
(NULL,'fifth district court of appeal','300 south beach st', 'Daytona', 'Fl',321158763, 3862258600,'5dca@us.fl.gov','leoncountycircuitcourt.gov/',null);

commit;


-- ---------------------------------
-- Data for table judge
-- ---------------------------------
Start transaction ;
Insert into judge
(per_id,crt_id,jud_salary,jud_years_in_practice,jud_notes)
values
(11,5,150000,10,null),
(12,4,185000,3,null),
(13,4,135000,2,null),
(14,3,170000,6,null),
(15,1,120000,1,null);

commit;

-- ---------------------------------
-- Data for table judge_hist
-- ---------------------------------
Start transaction ;
Insert into judge_hist
(jhs_id,per_id,jhs_crt_id,jhs_date,jhs_type,jhs_salary,jhs_notes)
values
(Null,11,3,'2009-01-16','i',130000,null),
(Null,12,2,'2010-05-27','i',140000,null),
(Null,13,5,'2000-01-02','i',115000,null),
(Null,13,4,'2005-07-05','i',135000,null),
(Null,14,4,'2008-12-09','i',155000,null),
(Null,15,1,'2011-03-17','i',120000,'freshman justice'),
(Null,11,5,'2010-07-05','i',150000,'assigned to another court'),
(null,12,4,'2012-10-08','i',165000,'became chief justice'),
(null,14,3,'2009-04-19',"i",170000,'reassigned to court based upon local area population growth');

commit;

-- ---------------------------------
-- Data for table assignment
-- ---------------------------------
Start transaction ;
SET FOREIGN_KEY_CHECKS=0;
Insert into assignment
(asn_id,per_cid,per_aid,cse_id,asn_notes)
values
(Null,1,6,7,Null),
(Null,2,6,6,Null),
(Null,3,7,2,Null),
(Null,4,8,2,Null),
(Null,5,9,5,Null),
(Null,1,10,1,Null),
(Null,2,6,3,Null),
(Null,3,7,8,Null),
(Null,4,8,8,Null),
(Null,5,9,8,Null),
(Null,4,10,4,Null);

commit;
SET FOREIGN_KEY_CHECKS=1;
-- populate person tablewith hashed and salted ssn numbers. must included salted valued in db
Drop procedure if exists CreatePersonSSN;
DELIMiTER $$
Create procedure CreatePersonSSN()
BEGIN

-- MYSQL Variables: //stackoverflow.com/questions/11754781/how-to-declare-a-variable-in-mysql

Declare x,y int;
SET x = 1 ;

-- dynamically set loop ending value (total number of persons)
select count(*) into y from person;

-- select y; display number pf [ersons only for testing

while x <= y do

-- give each person a unique randomized salt, and hashed and salted randomized ssn

SET @salt=RANDOM_BYTES(64);-- salt included unique random bytes for each user when looping
Set @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111;-- Random nine digit social inclusive
set @ssn=unhex(sha2(concat(@salt,@ran_num),512));

update person
set per_ssn=@ssn,per_salt=@salt
where per_id=x;

set x =x +1;

end while;

end $$

delimiter ;
call CreatePersonSSN();
